﻿using UnityEngine;
using System.Collections;

public class animate : MonoBehaviour {
	//Arms aquired from blender artist Kuronus
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("w")) {
			if (!Input.GetKey("space")){
			GetComponent<Animator> ().SetBool ("run", true);
			//Debug.Log ("true");
			}
		}

		if (Input.GetKeyUp("w")) {
			GetComponent<Animator> ().SetBool ("run", false);
			//Debug.Log ("false");
		}

		if (Input.GetKeyDown ("space")) {
			GetComponent<Animator> ().SetBool ("jump", true);
			GetComponent<Animator> ().SetBool ("run", false);
			//Debug.Log ("jumped");
			StartCoroutine ("WaitForSeconds");
			//Debug.Log ("waited");
		}

		if (Input.GetKey("e")) {
			GetComponent<Animator> ().SetBool ("slash", true);
			//Debug.Log ("attack");
			StartCoroutine ("knifestrike");
		}
	}

	IEnumerator knifestrike(){
		yield return new WaitForSeconds (0.7f);
		GetComponent<Animator> ().SetBool ("slash", false);
		//Debug.Log ("reset knife");
	}

	IEnumerator WaitForSeconds(){
		yield return new WaitForSeconds (0.7f);
		GetComponent<Animator> ().SetBool ("jump", false);
		//Debug.Log ("reset");
	}
}
