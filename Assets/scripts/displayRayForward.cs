using UnityEngine;
using System.Collections;

public class displayRayForward : MonoBehaviour {
	
	public int rayLength =10;


	
	// Use this for initialization
	void Start () {
	 	
	}
	
	// Update is called once per frame
	void Update () {
	 	Color fwdRayColor=Color.green;
		float fwdRayLength = 10;
		
		RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
		{
            float distanceToHit = hit.distance;
			Object itemHit=hit.collider;
			if (distanceToHit < fwdRayLength)
			{
				fwdRayColor=Color.red;
				fwdRayLength=distanceToHit;
				
			
			}
		}
		
		
		Vector3 height = this.transform.position;
		height.y += 1;

		
		Debug.DrawRay(height,fwdRayLength*this.transform.forward,fwdRayColor);
	

		Vector3 vLeft = Vector3.Cross(this.transform.forward,Vector3.up);
		Vector3 vRight = Vector3.Cross (this.transform.forward, Vector3.down);
		Vector3 v45Left = vLeft + this.transform.forward;
		v45Left.Normalize();
		
		Vector3 v45Right= vRight + this.transform.forward;
		v45Right.Normalize();
		
		Debug.DrawRay(height,rayLength*vLeft,Color.blue);
		Debug.DrawRay(height,rayLength*vRight,Color.blue);
		Debug.DrawRay(height,rayLength*v45Left,Color.red);
		Debug.DrawRay(height,rayLength*v45Right,Color.red);
	}
}
