﻿using UnityEngine;
using System.Collections;

public class spawn : MonoBehaviour {
	public GameObject master;
	bool flip  = false;
	public string trigger;
 	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("tribesman") != null) {
			trigger = GameObject.Find ("tribesman").GetComponent<FSM> ().mCurrentState;
			if (trigger == "dead"&& flip == false) {
				flip = true;
				go ();
			}
		}
	}

	void make(){
		Instantiate (master, this.gameObject.transform.position, this.transform.rotation);
	}

	void go(){
			InvokeRepeating ("make", 5, 15);
	}
}
