﻿using UnityEngine;
using System.Collections;

public class spin : MonoBehaviour {
	public GameObject pl;
	public GameObject knife;
	// Use this for initialization
	void OnTriggerEnter () {
		pl.GetComponent<Animator> ().SetBool ("knife", true);
		knife.SetActive (true);
		Destroy (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (Vector3.up * Time.deltaTime * 60);
	}
}
