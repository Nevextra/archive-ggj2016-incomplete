﻿using UnityEngine;
using System.Collections;

public class knifeAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("e")) {
				GetComponent<Animator> ().SetBool ("knife", true);
				//Debug.Log ("attack");
				StartCoroutine ("WaitForSeconds");
			}
	}

	IEnumerator WaitForSeconds(){
		yield return new WaitForSeconds (0.7f);
		GetComponent<Animator> ().SetBool ("knife", false);
		//Debug.Log ("reset");
	}
}
