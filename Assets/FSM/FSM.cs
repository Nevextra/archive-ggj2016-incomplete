﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FSM : MonoBehaviour
{
    public string mCurrentState;
    private Dictionary<string, FSMState> statesList =
            new Dictionary<string, FSMState>();

    // Use this for initialization
    void Start()
    {
        Component[] templist;
        templist = GetComponents<FSMState>();
        Debug.Log(templist.ToString());
        int statesLoaded = 0;

        foreach (FSMState state in templist)
        {
            //Debug.Log("//component found:" + state.strStateName);
            statesList.Add(state.strStateName, state as FSMState);
            statesLoaded++;
            state.enabled = false;
            state.myParentFSM = this;
        }
        mCurrentState = "";

       SetState("alive");
    }

    // Update is called once per frame
    void Update()
    {
     
    }

    public void SetState(string state)
    {
        if (mCurrentState != "")
        {
            (statesList[mCurrentState] as MonoBehaviour).enabled = false;
        }

        //update current state variable
        mCurrentState = state;
        //switch on new state
       (statesList[mCurrentState] as MonoBehaviour).enabled = true;
    }
		

}