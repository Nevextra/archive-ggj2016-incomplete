﻿using UnityEngine;
using System.Collections;

public class FSMPursue : FSMState {
	NavMeshAgent agent;
	public static bool died = false;
	public Vector3 playerPos;
	// Use this for initialization
	void Awake () {
        base.Awake();
        strStateName = "pursue";
		agent = GetComponent<NavMeshAgent> ();

    }
	
	// Update is called once per frame
	void Update () {
        //action that state will take
		playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        if (CanSeePlayer())
        {
			if (!GetIfPlayerWithinRange (2.4f)) {
				GetComponent<Animator> ().SetBool ("run", true);
				FacePlayer ();
				agent.SetDestination (playerPos);
			}

			if (GetIfPlayerWithinRange(2.4f)) {
				FacePlayer ();
				agent.ResetPath ();
				GetComponent<Animator> ().SetBool ("run", false);
				GetComponent<Animator> ().SetBool ("alert", false);
				GetComponent<Animator> ().SetBool ("punch", true);
				StartCoroutine ("reset");
			}



			/*if (Input.GetKey ("k")) {
				killed = true;
			}*/
        }

		if (!CanSeePlayer()) {
			myParentFSM.SetState ("alert");
		}

		if (died == true) {
			agent.ResetPath ();
			myParentFSM.SetState ("dead");
		}
    }


	IEnumerator reset(){
		yield return new WaitForSeconds (1);
		GetComponent<Animator> ().SetBool ("punch", false);
	}
}
