﻿using UnityEngine;
using System.Collections;

public class FSMEvade : FSMState {
	NavMeshAgent agent;
	// Use this for initialization
	void Awake () {
        base.Awake();
        strStateName = "evade";
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
        //action that state will take
    }
}
