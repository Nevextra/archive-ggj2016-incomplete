﻿using UnityEngine;
using System.Collections;

public class FSMAlive : FSMState {
	Component[] boneRig;
	// Use this for initialization
	void Awake () {
		base.Awake();
		strStateName = "alive";
		boneRig = GetComponentsInChildren<Rigidbody> ();
		SetRagdollState (false);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("living");
		if (this.gameObject.name == "hunters(Clone)" || this.gameObject.name == "hunters") {
			myParentFSM.SetState ("frenzy");
		} else {
			myParentFSM.SetState ("patrol");
		}
	}

	void SetRagdollState(bool onoff)
	{
		//switch rigid bodies on
		foreach( Rigidbody item in boneRig)
		{
			item.isKinematic = !onoff;
			item.gameObject.GetComponent<Collider> ().enabled = onoff;
		}
		//disable animation controller
		GetComponent<Animator> ().enabled = !onoff;
		GetComponent<Collider> ().enabled = !onoff;
		GetComponent<Rigidbody>().isKinematic = onoff;
	}
}
