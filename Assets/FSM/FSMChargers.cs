﻿using UnityEngine;
using System.Collections;

public class FSMChargers : FSMState {
	NavMeshAgent agent;
	public bool token;
	public Vector3 playerPos;
	void Awake () {
		base.Awake();
		strStateName = "frenzy";
		agent = GetComponent<NavMeshAgent> ();
		token = false;
	}
	
	// Update is called once per frame
	void Update () {
		playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
		if (!GetIfPlayerWithinRange (2.4f)) {
			GetComponent<Animator> ().SetBool ("run", true);
			agent.SetDestination (playerPos);
		}

		if (GetIfPlayerWithinRange (2.4f)){
			FacePlayer ();
			agent.ResetPath ();
			GetComponent<Animator> ().SetBool ("run", false);
			GetComponent<Animator> ().SetBool ("alert", false);
			GetComponent<Animator> ().SetBool ("punch", true);
			StartCoroutine ("reset");
		}

		if (token == true) {
			agent.ResetPath ();
			myParentFSM.SetState ("dead");
		}
			
	}
		

	IEnumerator reset(){
		yield return new WaitForSeconds (1);
		GetComponent<Animator> ().SetBool ("punch", false);
	}
}
