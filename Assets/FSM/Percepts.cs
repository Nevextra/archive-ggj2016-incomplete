﻿using UnityEngine;
using System.Collections;

public class Percepts : MonoBehaviour {
	public GameObject pl;
	// Use this for initialization
	protected void Awake () {
		pl = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void MoveForwards()
    {
        transform.Translate(0f, 0f, 0.07f, Space.Self);
    }

    public bool GetIfPlayerWithinRange(float range)
	{
		return (GetDistanceToPlayer() < range);
	}
	
	public float GetDistanceToPlayer()
	{
		
		float distance=Vector3.Distance(pl.transform.position, transform.position);
		//Debug.Log(distance);
		return distance;
	}
	
	public void FacePlayer()
	{
		float angle=GetAngleToPlayer();
		transform.Rotate(0,-GetAngleToPlayer(),0);
	}
	
	public void FaceAwayFromPlayer()
	{
		float angle=180.0f+GetAngleToPlayer();
		transform.Rotate(0,-angle,0);
	}
	
	public Vector3 GetVectorToPlayer()
	{
		Vector3 targetDir= pl.transform.position - transform.position;
		targetDir.y=0;
		return targetDir;
	}
	
	public float GetAngleToPlayer()
	{
		//get relative position of player
		Vector3 targetDir= GetVectorToPlayer();
		
		//forward vector of NPC
		Vector3 forward = transform.forward;
		forward.y=0;
		//calc cangle between 
		float angle = Vector3.Angle(forward,targetDir);
		
		//is angle positive?
		//compute cross product
		Vector3 cross=Vector3.Cross(targetDir,forward);
		if (cross.y<0) angle *=-1.0f;
		return angle;
	}

	public void FacePoint(Vector3 pt)
	{
		float angle=GetAngleToPoint(pt);
		transform.Rotate(0,-angle,0);
	}
	
	public Vector3 GetVectorToPoint(Vector3 pt)
	{
		Vector3 targetDir= pt - transform.position;
		targetDir.y=0;
		return targetDir;
	}
	
	public float GetDistanceToPoint(Vector3 pt)
	{
		
		float distance=Vector3.Distance(pt, transform.position);
		//Debug.Log(distance);
		return distance;
	}
	
	public float GetAngleToPoint(Vector3 pt)
	{
		//get relative position of player
		Vector3 targetDir= GetVectorToPoint(pt);
		
		//forward vector of NPC
		Vector3 forward = transform.forward;
		forward.y=0;
		//calc cangle between 
		float angle = Vector3.Angle(forward,targetDir);
		
		//is angle positive?
		//compute cross product
		Vector3 cross=Vector3.Cross(targetDir,forward);
		if (cross.y<0) angle *=-1.0f;
		return angle;
	}

    public bool CanSeePlayer()
    {
        float rayLength = 10.0f;
        RaycastHit hit;
        Vector3 rayStart = this.transform.position;
		rayStart.y += 1;
        Vector3 rayDirection = GetVectorToPlayer();
		Debug.DrawRay(rayStart,rayDirection*rayLength,Color.green);
        if (Physics.Raycast(rayStart, rayDirection, out hit, rayLength))
        {
            //Debug.Log("something in front : " + hit.collider.name);
            if (hit.collider.tag == "Player")
                return true;
        }
        return false;
    }

}
