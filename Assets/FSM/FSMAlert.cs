﻿using UnityEngine;
using System.Collections;

public class FSMAlert : FSMState {
	NavMeshAgent agent;
	void Awake () {
		base.Awake();
		strStateName = "alert";
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!CanSeePlayer ()) {
			myParentFSM.SetState ("patrol");
		}
		else
		{
			FacePlayer();
			GetComponent<Animator> ().SetBool ("alert", true);
			GetComponent<Animator> ().SetBool ("punch", false);
			StartCoroutine ("GetHim");
		}
	}

	IEnumerator GetHim(){
		yield return new WaitForSeconds (2);
		myParentFSM.SetState ("pursue");
	}
}
