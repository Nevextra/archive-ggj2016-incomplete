﻿using UnityEngine;
using System.Collections;

public class FSMPatrol : FSMState {
    public Path mypath;
	public static bool killed = false;
    private int targetPoint = 0;
	NavMeshAgent agent;
    // Use this for initialization
    void Awake () {
        base.Awake();
        strStateName = "patrol";
		agent = GetComponent<NavMeshAgent> ();
    }
	
	// Update is called once per frame
	void Update () {
       //action that state will take
        if (!CanSeePlayer())
        {
            FollowPath(mypath);
			GetComponent<Animator> ().SetBool ("run", true);
        }
        else
        {
			agent.ResetPath();
			GetComponent<Animator>().SetBool("run",false);
            myParentFSM.SetState("alert");
        }
			
		if (killed == true) {
			agent.Stop();
			myParentFSM.SetState ("dead");
		}

		if (this.gameObject.name == "hunters(Clone)"||this.gameObject.name == "hunters") {
			myParentFSM.SetState ("frenzy");
		}
    }

    protected void FollowPath(Path p)
    {

        //get current target point pt
        Vector3 currentTargetPt = p.GetPositionOfWP(targetPoint);
        //face point pt
        FacePoint(currentTargetPt);
        //move towards point pt
        float speed = 5.0f;
        //transform.Translate(Vector3.forward * speed * Time.deltaTime);
		agent.SetDestination(currentTargetPt);


        if (GetDistanceToPoint(currentTargetPt) < 1.0f)
        {
            targetPoint++;
            if (p.GetNumWaypoints() == targetPoint)
            {
                targetPoint = 0;
            }

        }

        //if we have reached end of array then wrap back to 0
        //}
    }
}
