﻿using UnityEngine;
using System.Collections;

public class FSMDead : FSMState {
	Component[] boneRig;
	NavMeshAgent agent;
	bool dead = false;
	// Use this for initialization
	void Awake () {
		base.Awake();
		strStateName = "dead";
		agent = GetComponent<NavMeshAgent> ();
		boneRig = GetComponentsInChildren<Rigidbody> ();
		//SetRagdollState (false);
	}
	
	// Update is called once per frame
	void Update()
	{
		if(myParentFSM.mCurrentState == ("dead"))
		{
			kill ();
		}

		if (Input.GetKey ("k")) {
			agent.Stop();
			myParentFSM.SetState ("dead");
		}
	}

	void SetRagdollState(bool onoff)
	{
		//switch rigid bodies on
		foreach( Rigidbody item in boneRig)
		{
			item.isKinematic = !onoff;
			item.gameObject.GetComponent<Collider> ().enabled = onoff;
		}
		//disable animation controller
		GetComponent<Animator> ().enabled = !onoff;
		GetComponent<Collider> ().enabled = !onoff;
		GetComponent<Rigidbody>().isKinematic = onoff;
	}
	// Update is called once per frame
	public void kill () {
		SetRagdollState (true);
		StartCoroutine ("gone");
	}

	IEnumerator gone(){
		yield return new WaitForSeconds (1);
		Destroy (this.gameObject);
	}
}
