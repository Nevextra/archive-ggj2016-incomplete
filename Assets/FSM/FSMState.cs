﻿using UnityEngine;
using System.Collections;

public class FSMState : Percepts{
    public string strStateName = "state";
    public FSM myParentFSM;

	// Use this for initialization
	protected void Awake () {
        base.Awake();
	    myParentFSM = GetComponent<FSM>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
